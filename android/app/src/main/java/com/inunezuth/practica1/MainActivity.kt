package com.inunezuth.practica1

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.io.IOException
import java.io.OutputStream
import java.util.*

class MainActivity : AppCompatActivity() {

    private val TAG:String = "MainActivity"
    lateinit var number1 : EditText
    lateinit var number2 : EditText
    lateinit var calculate : Button
    lateinit var result : TextView
    var number:Boolean = true
    var counter:Int = 5

   val mUUID: UUID  = UUID.fromString("000001101-0000-1000-8000-00805F9B34FB")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            number1 = findViewById(R.id.tv2)
            number2 = findViewById(R.id.tv3)
            calculate = findViewById(R.id.buttonCalculate)


        var btAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        Log.v(TAG,"Bonded devices are: "  + btAdapter.bondedDevices.toString())

        var hc05: BluetoothDevice = btAdapter.getRemoteDevice("00:20:10:00:0E:73")
        var btSocket: BluetoothSocket = hc05.createRfcommSocketToServiceRecord(mUUID)


        Log.v(TAG, "Socket is: " + btSocket.toString())

        Log.v(TAG, "Device name is: " + hc05.name.toString())
        do {
            try{
                btSocket.connect()
                Log.v(TAG, "Connected?" + btSocket.isConnected)
            }
            catch (e: IOException) {
                Log.e(TAG, e.toString())
            }
            counter --
        }

        while (counter >0 && !btSocket.isConnected)



        calculate.setOnClickListener {


            calculateNumbers()

            if (number) {
                try {
                    var outputStream: OutputStream = btSocket.outputStream
                    var estado: Int = 1
                    outputStream.write(estado)
                } catch (e: IOException) {
                    Log.e(TAG, e.toString())
                }
            } else {
                1
                try {
                    var outputStream: OutputStream = btSocket.outputStream
                    var estado: Int = 2
                    outputStream.write(estado)
                } catch (e: IOException) {
                    Log.e(TAG, e.toString())
                }
            }

        }
                }




        fun calculateNumbers() {
            var num1 : Int = 0
            var num2 : Int = 0

            num1 = number1.text.toString().toInt()
            num2 = number2.text.toString().toInt()

            if(num1 > num2){
                number=true
                Log.v(TAG, "Verde:  " + num1)

            } else {
                number=false
                Log.v(TAG, "Rojo:  " + num2)
            }
        }
    }
