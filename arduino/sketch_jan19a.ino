#include <SoftwareSerial.h>

SoftwareSerial bluetooth(10,11);
int LEDVerde=3;
int LEDRojo=5;
String estado;

void setup()
{
  bluetooth.begin(9600);
  Serial.begin(9600);
  pinMode(LEDVerde, OUTPUT);
  pinMode(LEDRojo, OUTPUT);
}
void loop()
{

  if(bluetooth.available())
  {
    estado=bluetooth.read();
    Serial.println(estado);
  
    if(estado=="1")
    {
      digitalWrite(LEDVerde, HIGH);
      digitalWrite(LEDRojo, LOW);
    }
    else if(estado=="2")
    {
      digitalWrite(LEDRojo, HIGH);
      digitalWrite(LEDVerde, LOW);
    }
  }
}
